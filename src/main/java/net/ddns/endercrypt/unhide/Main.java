package net.ddns.endercrypt.unhide;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

import org.kohsuke.args4j.CmdLineParser;

import net.ddns.endercrypt.unhide.application.LaunchArguments;
import net.ddns.endercrypt.unhide.application.UnHide;

public class Main
{
	private static Scanner scanner = new Scanner(System.in);

	public static void main(CmdLineParser cmdParser, LaunchArguments arguments) throws IOException
	{
		// print version
		if (arguments.isVersion())
		{
			System.out.println(UnHide.class.getSimpleName() + " by " + UnHide.author + " Version " + UnHide.version);
			ExitCode.EMPTY_SUCCESS.EndApplication();
		}

		// print help
		if (arguments.isHelp())
		{
			System.out.println("java -jar " + UnHide.filename.getName() + " [Options] [Files ...]");
			cmdParser.printUsage(System.out);
			System.out.println();
			System.out.println("Exit Codes: ");
			for (ExitCode exitCode : ExitCode.getExitCodes())
			{
				System.out.println("\t" + (exitCode.getCode() >= 0 ? " " : "") + exitCode.getCode() + " = " + exitCode.getDescription());
			}
			ExitCode.EMPTY_SUCCESS.EndApplication();
		}

		// arguments
		List<String> stringArgs = arguments.isPipe() ? getPipe() : arguments.getArguments();
		List<Path> paths = stringArgs.stream().map(s -> Paths.get(s)).collect(Collectors.toList());

		// basic args check
		if (paths.size() == 0)
		{
			System.out.println("No arguments found!");
			ExitCode.NO_ARGUMENTS.EndApplication();
		}

		// remove unexistant files
		{
			Iterator<Path> iterator = paths.iterator();
			while (iterator.hasNext())
			{
				Path path = iterator.next();
				if (Files.exists(path) == false)
				{
					if (arguments.isIgnoreUnexistant() == false)
					{
						System.err.println(path.toString() + " does not exist! aborting.");
						ExitCode.FILE_NOT_EXIST.EndApplication();
					}
					iterator.remove();
				}
			}
			if (paths.size() == 0)
			{
				ExitCode.ARGUMENTS_FILTERED.EndApplication();
			}
		}

		// remove visible files
		{
			Iterator<Path> iterator = paths.iterator();
			while (iterator.hasNext())
			{
				Path path = iterator.next();
				if (Files.isHidden(path) == false)
				{
					if (arguments.isIgnoreVisible() == false)
					{
						System.err.println(path.toString() + " does not hidden! aborting.");
						ExitCode.NOT_HIDDEN.EndApplication();
					}
					iterator.remove();
				}
			}
			if (paths.size() == 0)
			{
				ExitCode.ARGUMENTS_FILTERED.EndApplication();
			}
		}

		// rename files
		for (Path path : paths)
		{
			String parent = path.getParent().toString();
			String filename = path.getFileName().toString();
			while (filename.startsWith("."))
			{
				filename = filename.substring(1);
			}
			Path target = Paths.get(parent, filename);
			String moveString = "mv " + path + " " + target;

			if (arguments.isDryRun())
			{
				if ((arguments.isOverwrite() == false) && (Files.exists(target)))
				{
					System.out.println("interrupt! " + path + " would not be allowed to be renamed as a file named " + target + " already existed");
					ExitCode.EMPTY_SUCCESS.EndApplication();
				}
				System.out.println(moveString);
			}
			else
			{
				if (Files.exists(target))
				{
					if (arguments.isOverwrite())
					{
						Files.delete(target);
					}
					else
					{
						System.err.println("file " + target + " already exists! interrupting");
						ExitCode.TARGET_EXISTS.EndApplication();
					}
				}
				if (arguments.isAsk())
				{
					ask_loop: while (true)
					{
						System.out.println("Really perform? ( y/n ) " + moveString);
						String inputString = scanner.nextLine();
						switch (inputString.toLowerCase())
						{
						case "y":
							break ask_loop;
						case "n":
							ExitCode.ASK_CANCELLED.EndApplication();
							break;
						default:
							continue;
						}
					}
				}
				Files.move(path, target);
			}
		}

		ExitCode.SUCCESS.EndApplication();
	}

	private static List<String> getPipe() throws IOException
	{
		List<String> result = new ArrayList<>();
		StringBuilder sb = new StringBuilder();

		while (true)
		{
			int raw = System.in.read();
			if ((raw == ' ') || (raw == '\n'))
			{
				result.add(sb.toString());
				sb.setLength(0);
			}
			if (raw == -1)
			{
				break;
			}
			sb.append((char) raw);
		}
		result.add(sb.toString());

		result = result.stream().map(s -> s.trim()).filter(s -> s.equals("")).collect(Collectors.toList());

		return result;
	}
}
