package net.ddns.endercrypt.unhide;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ExitCode implements Comparable<ExitCode>
{
	private static List<ExitCode> exitCodes = new ArrayList<>();

	public static final ExitCode SUCCESS = new ExitCode(0, "The command executed successfully");
	public static final ExitCode EMPTY_SUCCESS = new ExitCode(1, "The command was instructed to only output information");

	public static final ExitCode COMMANDLINE_ARGUMENTS_INVALID = new ExitCode("supplied command options where not valid");
	public static final ExitCode UNCAUGHT_EXCEPTION = new ExitCode("Uncaught internal exception occured");
	public static final ExitCode NO_ARGUMENTS = new ExitCode("no file arguments where provided");
	public static final ExitCode ARGUMENTS_FILTERED = new ExitCode("all arguments where filtered out (unexistant, visible etc)");
	public static final ExitCode FILE_NOT_EXIST = new ExitCode("A file marked for renaming did not exist (String mode)");
	public static final ExitCode NOT_HIDDEN = new ExitCode("A file marked for renaming was not hidden");
	public static final ExitCode ASK_CANCELLED = new ExitCode("user cancelled the renaming when asked");
	public static final ExitCode TARGET_EXISTS = new ExitCode("the target file of a rename operation already existed");

	static
	{
		Collections.sort(exitCodes);
	}

	private static int failure_code_counter = -1;

	public static List<ExitCode> getExitCodes()
	{
		return Collections.unmodifiableList(exitCodes);
	}

	private int code;
	private String description;

	private ExitCode(String description)
	{
		this(--failure_code_counter, description);
	}

	private ExitCode(int code, String description)
	{
		this.code = code;
		this.description = description;

		exitCodes.add(this);
	}

	public int getCode()
	{
		return code;
	}

	public String getDescription()
	{
		return description;
	}

	@Override
	public int compareTo(ExitCode other)
	{
		return -Integer.compare(this.getCode(), other.getCode());
	}

	public void EndApplication()
	{
		System.exit(getCode());
	}
}
