package net.ddns.endercrypt.unhide.application;

import java.io.File;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

import net.ddns.endercrypt.unhide.ExitCode;
import net.ddns.endercrypt.unhide.Main;

public class UnHide
{
	public static final File filename = new File(UnHide.class.getProtectionDomain().getCodeSource().getLocation().getPath());

	public static final String author = "Magnus Gunnarsson";
	public static final String version = "0.1";

	public static void main(String[] args)
	{
		// launch arguments
		LaunchArguments launchArguments = new LaunchArguments();
		CmdLineParser parser = new CmdLineParser(launchArguments);
		try
		{
			parser.parseArgument(args);
		}
		catch (CmdLineException e)
		{
			System.err.println("Error: " + e.getMessage());
			System.err.println("java -jar " + filename.getName() + " [Options] [Files ...]");
			parser.printUsage(System.err);
			ExitCode.COMMANDLINE_ARGUMENTS_INVALID.EndApplication();
		}

		// launch
		try
		{
			Main.main(parser, launchArguments);
		}
		catch (Exception e)
		{
			System.err.println("Fatal application exception!");
			e.printStackTrace();
			ExitCode.UNCAUGHT_EXCEPTION.EndApplication();
		}

		// finish
		ExitCode.SUCCESS.EndApplication();
	}
}
