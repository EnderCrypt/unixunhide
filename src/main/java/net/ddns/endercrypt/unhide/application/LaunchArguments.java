package net.ddns.endercrypt.unhide.application;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.Option;

public class LaunchArguments
{
	@Option(name = "-version", aliases = { "--version", "-v" }, required = false, usage = "prints the version number rather than perform anything")
	private boolean version = false;

	public boolean isVersion()
	{
		return version;
	}

	@Option(name = "-help", aliases = { "--help", "-h" }, required = false, usage = "prints the help information rather than perform anything")
	private boolean help = false;

	public boolean isHelp()
	{
		return help;
	}

	@Option(name = "-ignore-visible", aliases = { "-iv" }, required = false, usage = "ignores any input files that arent hidden, rather than aborting")
	private boolean ignoreVisible = false;

	public boolean isIgnoreVisible()
	{
		return ignoreVisible;
	}

	@Option(name = "-ignore-unexistant", aliases = { "-iu" }, required = false, usage = "verifies that all file arguments exists, prior to renaming")
	private boolean ignoreUnexistant = true;

	public boolean isIgnoreUnexistant()
	{
		return ignoreUnexistant;
	}

	@Option(name = "-ask", aliases = { "-a" }, forbids = { "-pipe", "-dry-run" }, required = false, usage = "asks to rename every file")
	private boolean ask = false;

	public boolean isAsk()
	{
		return ask;
	}

	@Option(name = "-pipe", aliases = { "-p" }, forbids = { "-ask" }, required = false, usage = "reads files from stdin rather than as arguments")
	private boolean pipe = false;

	public boolean isPipe()
	{
		return pipe;
	}

	@Option(name = "-overwrite", aliases = { "-o" }, required = false, usage = "attempts to overwrite any filename conflicts")
	private boolean overwrite = false;

	public boolean isOverwrite()
	{
		return overwrite;
	}

	@Option(name = "-dry-run", aliases = { "-d", "-dr" }, forbids = { "-ask" }, required = false, usage = "pretends to perform renaming (verbosly) whitout actually doing it")
	private boolean dryRun = false;

	public boolean isDryRun()
	{
		return dryRun;
	}

	@Argument
	private List<String> arguments = new ArrayList<>();

	public List<String> getArguments()
	{
		return Collections.unmodifiableList(arguments);
	}

}
