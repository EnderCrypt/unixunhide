UnHide by Magnus Gunnarsson Version 0.1<br>
<br>
Basic java application for mass renaming files to remove the initial dot, making the files visible again<br>
<br>
note: you may just net to put a bash script in a bin dir like /usr/local/bin which launches: java -jar UnHide.jar %@<br>
<br>
~~~~
$ unhide --help
java -jar UnHide.jar [Options] [Files ...]
 -ask (-a)                : asks to rename every file (default: false)
 -dry-run (-d, -dr)       : pretends to perform renaming (verbosly) whitout
                            actually doing it (default: false)
 -help (--help, -h)       : prints the help information rather than perform
                            anything (default: true)
 -ignore-visible (-i)     : ignores any input files that arent hidden, rather
                            than aborting (default: true)
 -overwrite (-o)          : attempts to overwrite any filename conflicts
                            (default: false)
 -pipe (-p)               : reads files from stdin rather than as arguments
                            (default: false)
 -strict (-s)             : verifies that all file arguments exists, prior to
                            renaming (default: true)
 -version (--version, -v) : prints the version number rather than perform
                            anything (default: false)

Return codes:
	 1 = The command was instructed to only output information
	 0 = The command executed successfully
	-1 = supplied command options where not valid
	-2 = Uncaught internal exception occured
	-3 = no file arguments where provided
	-4 = A file marked for renaming did not exist (String mode)
	-5 = A file marked for renaming was not hidden
	-6 = user cancelled the renaming when asked
	-7 = the target file of a rename operation already existed
~~~~
<br>
<br>